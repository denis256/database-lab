AWSTemplateFormatVersion: 2010-09-09
Description: >-
  AWS CloudFormation template DLE_Instance_Host: Creates a single EC2 instance based on Database Lab Engine (DLE) AMI,
  configures DLE, launches the data retrieval process, eventually making it possible to create thin clones using DLE API, CLI, or UI.
  You will be billed for the AWS resources used if you create a stack from this template.
Metadata:
  AWS::CloudFormation::Interface:
    ParameterGroups:
      -
        Label:
          default: "Amazon EC2 configuration"
        Parameters:
          - InstanceType
          - ZFSVolumeSize
          - SSHLocation
          - VPC
          - Subnet
          - KeyName
      - Label:
          default: "TLS certificate configuration"
        Parameters:
          - CertificateSubdomain
          - CertificateHostedZone
          - CertificateEmail
      -
        Label:
          default: "Database Lab Engine (DLE) configuration"
        Parameters:
          - DLERetrievalRefreshTimetable
          - PostgresDumpParallelJobs
          - DLEVerificationToken
          - DLEDebugMode
      -
        Label:
          default: "Source PostgreSQL parameters"
        Parameters:
          - SourcePostgresHost
          - SourcePostgresPort
          - SourcePostgresUsername
          - SourcePostgresPassword
          - SourcePostgresDBName
          - SourcePostgresVersion
          - PostgresConfigSharedPreloadLibraries
          - SourcePostgresDBList
    ParameterLabels:
      KeyName:
        default: "Key pair"
      InstanceType:
        default: "Instance type"
      SSHLocation:
        default: "Connection source IP range"
      ZFSVolumeSize:
        default: "EBS volume size in GB for ZFS"
      CertificateSubdomain:
        default: "Certificate subdomain"
      CertificateHostedZone:
        default: "Hosted zone"
      CertificateEmail:
        default: "Certificate email"
      DLEDebugMode:
        default: "DLE debug mode"
      DLEVerificationToken:
        default: "DLE verification token"
      DLERetrievalRefreshTimetable:
        default: "DLE retrieval refresh timetable"
      PostgresDumpParallelJobs:
        default: "Number of pg_dump jobs"
      SourcePostgresDBName:
        default: "Database name"
      VPC:
        default: "VPC security group"
      Subnet:
        default: "Subnet"
      SourcePostgresVersion:
        default: "Postgres version"
      SourcePostgresHost:
        default: "Host name or IP"
      SourcePostgresPort:
        default: "Port"
      SourcePostgresUsername:
        default: "User name"
      SourcePostgresPassword:
        default: "Password"
      PostgresConfigSharedPreloadLibraries:
        default: "shared_preload_libraries parameter"
      SourcePostgresDBList:
        default: "Comma separated list of databases to copy"

Parameters:
  Subnet:
    Description: Subnet to attach EC2 machine.
    Type: AWS::EC2::Subnet::Id
  VPC:
    Description: VPC to attach EC2 machine.
    Type: AWS::EC2::VPC::Id
    ConstraintDescription: Can contain only ASCII characters and can not be empty.
  KeyName:
    Description: Name of an existing EC2 KeyPair to enable SSH access to the instance
    Type: 'AWS::EC2::KeyPair::KeyName'
    ConstraintDescription: Can contain only ASCII characters and can not be empty.
  InstanceType:
    Description: DLE EC2 instance type
    Type: String
    Default: t2.small
    AllowedValues:
      - t1.micro
      - t2.nano
      - t2.micro
      - t2.small
      - t2.medium
      - t2.large
      - m1.small
      - m1.medium
      - m1.large
      - m1.xlarge
      - m2.xlarge
      - m2.2xlarge
      - m2.4xlarge
      - m3.medium
      - m3.large
      - m3.xlarge
      - m3.2xlarge
      - m4.large
      - m4.xlarge
      - m4.2xlarge
      - m4.4xlarge
      - m4.10xlarge
      - c1.medium
      - c1.xlarge
      - c3.large
      - c3.xlarge
      - c3.2xlarge
      - c3.4xlarge
      - c3.8xlarge
      - c4.large
      - c4.xlarge
      - c4.2xlarge
      - c4.4xlarge
      - c4.8xlarge
      - g2.2xlarge
      - g2.8xlarge
      - r3.large
      - r3.xlarge
      - r3.2xlarge
      - r3.4xlarge
      - r3.8xlarge
      - i2.xlarge
      - i2.2xlarge
      - i2.4xlarge
      - i2.8xlarge
      - d2.xlarge
      - d2.2xlarge
      - d2.4xlarge
      - d2.8xlarge
      - hi1.4xlarge
      - hs1.8xlarge
      - cr1.8xlarge
      - cc2.8xlarge
      - cg1.4xlarge
    ConstraintDescription: must be a valid EC2 instance type.
  SSHLocation:
    Description: CIDR in format x.x.x.x/32 to allow one specific IP address access, 0.0.0.0/0 to allow all IP addresses access, or another CIDR range
    Type: String
    MinLength: '9'
    MaxLength: '18'
    AllowedPattern: '(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})/(\d{1,2})'
    ConstraintDescription: Must be a valid IP CIDR range of the form x.x.x.x/x
  ZFSVolumeSize:
    Description: The size of the EBS volumes used for DLE ZFS pool
    Type: Number
    Default: 40
  CertificateSubdomain:
    Description: Subdomain to obtain a TLS certificate for (for example, dle)
    Type: String
  CertificateHostedZone:
    Description: Hosted zone to obtain a TLS certificate for (for example, example.com)
    Type: String
  CertificateEmail:
    Description: Email address for important account notifications about the issued TLS certificate
    Type: String
    AllowedPattern: '^$|[^\s@]+@[^\s@]+\.[^\s@]+'
    Default: ''
    ConstraintDescription: Must be a valid email of the form \'user@example.com\'
  DLEDebugMode:
    Description: Enables DLE debug mode
    Type: String
    Default: True
    AllowedValues:
      - True
      - False
  DLEVerificationToken:
    Description: DLE verification token
    Type: String
    Default: "example-verification-token"
    MinLength: '9'
    MaxLength: '32'
  DLERetrievalRefreshTimetable:
    Description: DLE refresh schedule on cron format
    Type: String
    Default: '0 0 * * *'
  SourcePostgresDBName:
    Description: Source database name. This parameter is used to connect to the database
    Type: String
    Default: 'postgres'
  SourcePostgresVersion:
    Description: Source database Postgres version
    Type: String
    Default: 12
    AllowedValues:
      - 9.6
      - 10
      - 11
      - 12
      - 13
      - 14
  SourcePostgresHost:
    Description: Source Postgres cluster host name or IP
    Type: String
    Default: ''
  SourcePostgresPort:
    Description: Source Postgres cluster port
    Type: Number
    MinValue: 1024
    MaxValue: 65535
    Default: 5432
  SourcePostgresUsername:
    Description: Source Postgres cluster username
    Type: String
    Default: postgres
  SourcePostgresPassword:
    Description: Source Postgres cluster password
    Type: String
    Default: ''
    NoEcho: true
  PostgresConfigSharedPreloadLibraries:
    Description: Source Postgres shared_preload_libraries value
    Type: String
    Default: ''
  PostgresDumpParallelJobs:
    Description: Number of jobs to run pg_dump against the source database
    Type: String
    Default: '1'
  SourcePostgresDBList:
    Description: List of database names on source for copy to DLE. Leave it empty to copy all accessible databases
    Type: String
    Default: ''
Mappings:
  AWSInstanceType2Arch:
    t1.micro:
      Arch: HVM64
    t2.nano:
      Arch: HVM64
    t2.micro:
      Arch: HVM64
    t2.small:
      Arch: HVM64
    t2.medium:
      Arch: HVM64
    t2.large:
      Arch: HVM64
    m1.small:
      Arch: HVM64
    m1.medium:
      Arch: HVM64
    m1.large:
      Arch: HVM64
    m1.xlarge:
      Arch: HVM64
    m2.xlarge:
      Arch: HVM64
    m2.2xlarge:
      Arch: HVM64
    m2.4xlarge:
      Arch: HVM64
    m3.medium:
      Arch: HVM64
    m3.large:
      Arch: HVM64
    m3.xlarge:
      Arch: HVM64
    m3.2xlarge:
      Arch: HVM64
    m4.large:
      Arch: HVM64
    m4.xlarge:
      Arch: HVM64
    m4.2xlarge:
      Arch: HVM64
    m4.4xlarge:
      Arch: HVM64
    m4.10xlarge:
      Arch: HVM64
    c1.medium:
      Arch: HVM64
    c1.xlarge:
      Arch: HVM64
    c3.large:
      Arch: HVM64
    c3.xlarge:
      Arch: HVM64
    c3.2xlarge:
      Arch: HVM64
    c3.4xlarge:
      Arch: HVM64
    c3.8xlarge:
      Arch: HVM64
    c4.large:
      Arch: HVM64
    c4.xlarge:
      Arch: HVM64
    c4.2xlarge:
      Arch: HVM64
    c4.4xlarge:
      Arch: HVM64
    c4.8xlarge:
      Arch: HVM64
    r3.large:
      Arch: HVM64
    r3.xlarge:
      Arch: HVM64
    r3.2xlarge:
      Arch: HVM64
    r3.4xlarge:
      Arch: HVM64
    r3.8xlarge:
      Arch: HVM64
    i2.xlarge:
      Arch: HVM64
    i2.2xlarge:
      Arch: HVM64
    i2.4xlarge:
      Arch: HVM64
    i2.8xlarge:
      Arch: HVM64
    d2.xlarge:
      Arch: HVM64
    d2.2xlarge:
      Arch: HVM64
    d2.4xlarge:
      Arch: HVM64
    d2.8xlarge:
      Arch: HVM64
    hi1.4xlarge:
      Arch: HVM64
    hs1.8xlarge:
      Arch: HVM64
    cr1.8xlarge:
      Arch: HVM64
    cc2.8xlarge:
      Arch: HVM64
  AWSRegionArch2AMI:
    eu-north-1:
      HVM64: ami-034e4aed1f3ca0c3c
    ap-south-1:
      HVM64: ami-0e659eacedfb041d7
    eu-west-3:
      HVM64: ami-08e35ca54e9190c97
    eu-west-2:
      HVM64: ami-0ecae7aba70abc116
    eu-west-1:
      HVM64: ami-042f1da51ef5fd484
    ap-northeast-3:
      HVM64: ami-01e9e9d567c32dd8f
    ap-northeast-2:
      HVM64: ami-090fe41b7122583e1
    ap-northeast-1:
      HVM64: ami-071b0fe046bc270b3
    sa-east-1:
      HVM64: ami-07a47547657f8dc18
    ca-central-1:
      HVM64: ami-0578cffaf594ab219
    ap-southeast-1:
      HVM64: ami-0f151d9dcf524c7c2
    ap-southeast-2:
      HVM64: ami-005263d8988e47a47
    eu-central-1:
      HVM64: ami-0b32ee6d741554a21
    us-east-1:
      HVM64: ami-088b2214cd2a232dc
    us-east-2:
      HVM64: ami-033774769e0f3cfc7
    us-west-1:
      HVM64: ami-07215a5d464906c4c
    us-west-2:
      HVM64: ami-05b2a489e553d9f0c

Conditions:
  CreateSubDomain:
    !Not [!Equals [!Ref CertificateHostedZone, '']]

Resources:
  ZFSVolume:
    Type: AWS::EC2::Volume
    DeletionPolicy: Snapshot
    Properties:
      Encrypted: True
      AvailabilityZone: !GetAtt DLEInstance.AvailabilityZone
      Size: !Ref ZFSVolumeSize
      Tags:
        -
          Key: Name
          Value: dle-zfs-volume
      VolumeType: gp2

  DLEInstance:
    Type: 'AWS::EC2::Instance'
    Properties:
      ImageId: !FindInMap
        - AWSRegionArch2AMI
        - !Ref 'AWS::Region'
        - !FindInMap
          - AWSInstanceType2Arch
          - !Ref InstanceType
          - Arch
      InstanceType: !Ref InstanceType
      SecurityGroupIds: !If
        - CreateSubDomain
        - - !GetAtt DLESecurityGroup.GroupId
          - !GetAtt DLEUISecurityGroup.GroupId
        - - !GetAtt DLESecurityGroup.GroupId
      KeyName: !Ref KeyName
      SubnetId: !Ref Subnet
      Tags:
        -
          Key: Name
          Value: "DLE Instance"
      UserData:
        Fn::Base64: !Sub |     # No more Fn::Join needed
          #!/bin/bash
          set -ex

          sleep 30

          sudo zpool create -f \
            -O compression=on \
            -O atime=off \
            -O recordsize=128k \
            -O logbias=throughput \
            -m /var/lib/dblab/dblab_pool \
            dblab_pool \
            /dev/xvdh

          dle_config_path="/home/ubuntu/.dblab/engine/configs"
          dle_meta_path="/home/ubuntu/.dblab/engine/meta"
          postgres_conf_path="/home/ubuntu/.dblab/postgres_conf"

          yq e -i '
          .global.debug=${DLEDebugMode} |
          .embeddedUI.host="" |
          .server.verificationToken="${DLEVerificationToken}" |
          .retrieval.refresh.timetable="${DLERetrievalRefreshTimetable}" |
          .retrieval.spec.logicalRestore.options.forceInit=true |
          .databaseContainer.dockerImage="postgresai/extended-postgres:${SourcePostgresVersion}" |
          .databaseConfigs.configs.shared_preload_libraries="${PostgresConfigSharedPreloadLibraries}" |
          .databaseContainer.dockerImage="postgresai/extended-postgres:${SourcePostgresVersion}" 
          ' $dle_config_path/server.yml

          yq e -i '
          .retrieval.spec.logicalDump.options.source.connection.host = "${SourcePostgresHost}" |
          .retrieval.spec.logicalDump.options.source.connection.port = ${SourcePostgresPort} |
          .retrieval.spec.logicalDump.options.source.connection.dbname="${SourcePostgresDBName}" |
          .retrieval.spec.logicalDump.options.source.connection.username = "${SourcePostgresUsername}" |
          .retrieval.spec.logicalDump.options.source.connection.password = "${SourcePostgresPassword}" |
          .retrieval.spec.logicalDump.options.parallelJobs = ${PostgresDumpParallelJobs} |
          .retrieval.spec.logicalRestore.options.configs.shared_preload_libraries = "${PostgresConfigSharedPreloadLibraries}"
          ' $dle_config_path/server.yml

          for i in $(echo ${SourcePostgresDBList} | sed "s/,/ /g")
          do
            yq e -i "
            .retrieval.spec.logicalDump.options.databases.$i = {}
            " $dle_config_path/server.yml
          done


          sudo docker run \
            --detach \
            --name dblab_server \
            --label dblab_control \
            --privileged \
            --publish 2345:2345 \
            --volume /var/run/docker.sock:/var/run/docker.sock \
            --volume /var/lib/dblab:/var/lib/dblab/:rshared \
            --volume /var/lib/dblab/dblab_pool/dump:/var/lib/dblab/dblab_pool/dump/:rshared \
            --volume $dle_config_path:/home/dblab/configs:ro \
            --volume $dle_meta_path:/home/dblab/meta \
            --volume $postgres_conf_path:/home/dblab/standard/postgres/control \
            --env DOCKER_API_VERSION=1.39 \
            --restart always \
            registry.gitlab.com/postgres-ai/database-lab/dblab-server:3.0.3

          if [ ! -z "${CertificateHostedZone}" ]; then
            export DOMAIN=${CertificateSubdomain}.${CertificateHostedZone}
            export USER_EMAIL=${CertificateEmail}
            export CERTIFICATE_EMAIL=${!USER_EMAIL:-'noreply@'$DOMAIN}

            sudo certbot certonly --standalone -d $DOMAIN -m $CERTIFICATE_EMAIL --agree-tos -n
            sudo cp /etc/letsencrypt/live/$DOMAIN/fullchain.pem /etc/envoy/certs/fullchain1.pem
            sudo cp /etc/letsencrypt/live/$DOMAIN/privkey.pem /etc/envoy/certs/privkey1.pem

          cat <<EOF > /etc/letsencrypt/renewal-hooks/deploy/envoy.deploy
          #!/bin/bash
          umask 0177
          export DOMAIN=${CertificateSubdomain}.${CertificateHostedZone}
          export DATA_DIR=/etc/envoy/certs/
          cp /etc/letsencrypt/live/$DOMAIN/fullchain.pem $DATA_DIR/fullchain1.pem
          cp /etc/letsencrypt/live/$DOMAIN/privkey.pem   $DATA_DIR/privkey1.pem
          EOF
            sudo chmod +x /etc/letsencrypt/renewal-hooks/deploy/envoy.deploy

            sudo systemctl enable envoy
            sudo systemctl start envoy
          fi

  MountPoint:
    Type: AWS::EC2::VolumeAttachment
    Properties:
      InstanceId: !Ref DLEInstance
      VolumeId: !Ref ZFSVolume
      Device: /dev/xvdh

  DLEElasticIP:
    Type: AWS::EC2::EIP
    Properties:
      Domain: vpc
      InstanceId: !Ref DLEInstance

  SubDomain:
    Type: AWS::Route53::RecordSet
    Condition: CreateSubDomain
    Properties:
      HostedZoneName: !Sub '${CertificateHostedZone}.'
      Comment: DNS name for DLE instance.
      Name: !Sub '${CertificateSubdomain}.${CertificateHostedZone}'
      Type: CNAME
      TTL: 60
      ResourceRecords:
        - !GetAtt DLEInstance.PublicDnsName
    DependsOn:
      - DLEInstance
      - DLEElasticIP

  DLESecurityGroup:
    Type: 'AWS::EC2::SecurityGroup'
    Properties:
      GroupDescription: Enable ssh access via port 22
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: 22
          ToPort: 22
          CidrIp: !Ref SSHLocation
      SecurityGroupEgress:
        - IpProtocol: -1
          CidrIp: '0.0.0.0/0'
      VpcId: !Ref VPC

  DLEUISecurityGroup:
    Type: 'AWS::EC2::SecurityGroup'
    Condition: CreateSubDomain
    Properties:
      GroupDescription: Enable ports to access DLE UI
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: 80
          ToPort: 80
          CidrIp: !Ref SSHLocation

        - IpProtocol: tcp
          FromPort: 443
          ToPort: 443
          CidrIp: !Ref SSHLocation

        - IpProtocol: tcp
          FromPort: 446
          ToPort: 446
          CidrIp: !Ref SSHLocation
      SecurityGroupEgress:
        - IpProtocol: -1
          CidrIp: '0.0.0.0/0'
      VpcId: !Ref VPC


Outputs:
  VerificationToken:
    Description: 'DLE verification token'
    Value: !Ref DLEVerificationToken

  DLE:
    Description: URL for newly created DLE instance
    Value: !Sub 'https://${CertificateSubdomain}.${CertificateHostedZone}'
    Condition: CreateSubDomain

  UI:
    Description: UI URL with a domain for newly created DLE instance
    Value: !Sub 'https://${CertificateSubdomain}.${CertificateHostedZone}:446'
    Condition: CreateSubDomain

  DNSName:
    Description: Public DNS name
    Value: !GetAtt DLEInstance.PublicDnsName

  EC2SSH:
    Description: SSH connection to the EC2 instance with Database Lab Engine
    Value: !Sub
      - 'ssh -i YOUR_PRIVATE_KEY ubuntu@${DNSName}'
      - DNSName: !GetAtt DLEInstance.PublicDnsName

  DLETunnel:
    Description: Create an SSH-tunnel to Database Lab Engine
    Value: !Sub
      - 'ssh -N -L 2345:${DNSName}:2345 -i YOUR_PRIVATE_KEY ubuntu@${DNSName}'
      - DNSName: !GetAtt DLEInstance.PublicDnsName

  UITunnel:
    Description: Create an SSH-tunnel to Database Lab UI
    Value: !Sub
      - 'ssh -N -L 2346:${DNSName}:2346 -i YOUR_PRIVATE_KEY ubuntu@${DNSName}'
      - DNSName: !GetAtt DLEInstance.PublicDnsName

  CloneTunnel:
    Description: Create an SSH-tunnel to Database Lab clones
    Value: !Sub
      - 'ssh -N -L CLONE_PORT:${DNSName}:CLONE_PORT -i YOUR_PRIVATE_KEY ubuntu@${DNSName}'
      - DNSName: !GetAtt DLEInstance.PublicDnsName
